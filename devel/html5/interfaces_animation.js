//kaj je trigger da bo uprabnik vedu da se je neki zgodil da ima v roki. 
//the shoes that follow me.
//nimamo podatkov, hocemo podatke relevantne. 
//kak je natancnost.

//animation_objects.js holds all animation objects.

//CONTROLING MOUSE COURSOR
var cursorContentVisible=0;
var cursorContentType=1; //1-video

//RENDERING
var scale=0; //scaleFactor=CanvasWidhtOnCoordinateCapture/1920. 
var canvasWidthInPercent=83.38; //80% of screen size

var renderInterval;
var episodes=[3, 9, 15, 20];

//MOUSE HOVER 
var cursorHoverX; //Current mouse location for canvas hover implementation
var cursorHoverY;

var player = {
    email:"guest@mail.com",
	name:"guest",
	picture_count:0,
	audio_count:0,
	lyrics_count:0,
	story_state:0,
	sound_ch1:true,
	sound_ch2:false,
	episodes:episodes,
	chat_state:0,
	rendering:false,
	b_t_s_state:0,
	delay:4000, //delay for mate voice over
	mate_talking: true,
	pause_sound_ch2_enabled: false //enable pause sound for ch2
};


var lockedItems=[];
var item={
	id:"mate_knjiga_leva_hotspot_lock",
	locked:0
}
lockedItems.push(item);
var item={
	id:"nikica_vhod_hotspot_lock",
	locked:0
}
lockedItems.push(item);

var item={
	id:"lada_lucijo_sac_hotspot_lock",
	locked:0
}
lockedItems.push(item);


/********************************************************************
 CANVAS RENDERING
/********************************************************************/

function lock_all_POIs(){
	for(var i=0; i<POIs.length;i++)
		POIs[i].status=0;
}
function show_POI(id){
	for(var i=0; i<POIs.length;i++){
		//console.log("POIs[i].id: "+POIs[i].id+" POIs[i] status: "+POIs[i].status);
		if(POIs[i].id==id){
			POIs[i].status=1;
			break;
		}
		
	}
}

function draw_mate_animation(ctx, scale){
	
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
	
	if(player.mate_talking){
		var centerX =ctx.canvas.width*60/239;
	    var centerY = ctx.canvas.height*60/100;
	    ctx.globalAlpha = 0.1;
		
	    for(var i=0; i<m_animation.length;i++){ 	 
		    if(m_animation[i].radius<m_animation[i].max)
		    	m_animation[i].radius+=m_animation[i].speed;
		    else
		    	m_animation[i].radius=m_animation[i].min;
		    
		   	draw_mate_animation
		    ctx.beginPath();
			ctx.arc(centerX, centerY, m_animation[i].radius, 0, 2 * Math.PI, false);
			ctx.lineWidth = 2;
			ctx.strokeStyle = '#FFFFFF';
			ctx.stroke();
		}
		ctx.restore();
	}
}

function drawPOI(index, ctx, scale){
	//chnage direction
	if(POIs[index].offset==POIs[index].points.length-1){
		POIs[index].startDirection=false;
		//console.log("startDirection: "+startDirection);
	}else if(POIs[index].offset==0){
		POIs[index].startDirection=true;
	}
	
	var srcImg;
	if(POIs[index].status==0)//locked
		srcImg=POIs[index].img
	else//new
		srcImg=POIs[index].imgNew;
	
	ctx.drawImage(	srcImg, 
					POIs[index].points[POIs[index].offset][0]*scale-POIs[index].img.width/2*scale,
					POIs[index].points[POIs[index].offset][1]*scale-POIs[index].img.height*scale,
					POIs[index].img.width*scale,POIs[index].img.height*scale);
			
	if(POIs[index].frameCounter==POIs[index].framesToSkip||POIs[index].framesToSkip==0){
		POIs[index].frameCounter=0;
		if(POIs[index].circlePath){
			if(POIs[index].startDirection==true)
				POIs[index].offset++;
			else
				POIs[index].offset=0;
		}else{
			if(POIs[index].startDirection==true)
				POIs[index].offset++;
			else
				POIs[index].offset--;
		}
	}
	POIs[index].frameCounter++;
}

function drawCloud(index, ctx, scale){
	
	//move back to beginning
	if(clouds[index].offset<clouds[index].ymin){
		clouds[index].offset=clouds[index].ymax;
		clouds[index].imgIndex+=1;
		
		//clouds[index].alpha=clouds[index].alphaStart;
		clouds[index].alpha=1.5;
		
		
		if(clouds[index].imgIndex==clouds[index].imgs.length)
			clouds[index].imgIndex=0;
		
		switch(index){
			case 0:
				clouds[index].n=randomIntFromInterval(-253,+153);
				clouds[index].speed=randomFloatFromInterval(1,1.5);
			break;
			case 1:
				clouds[index].n=randomIntFromInterval(-733,-233);
				clouds[index].speed=randomFloatFromInterval(1.5,2.5);
			break;
			case 2:
				clouds[index].n=randomIntFromInterval(-1181,-981);
				clouds[index].speed=randomFloatFromInterval(0.5,0.8);
			break;
		}
	}
	
	//calc cloud position based on linear equation y=kx+n
	y=clouds[index].offset;
	x=y/clouds[index].k-clouds[index].n;
	
	x=x*scale;
	y=y*scale;
	
	//console.log(clouds[index].alpha);
	ctx.globalAlpha = clouds[index].alpha;
	
	ctx.drawImage(	clouds[index].imgs[clouds[index].imgIndex], 
					x-clouds[index].imgs[clouds[index].imgIndex].width/2*scale,
					y-clouds[index].imgs[clouds[index].imgIndex].height/2*scale,
					clouds[index].imgs[clouds[index].imgIndex].width*scale*2,
					clouds[index].imgs[clouds[index].imgIndex].height*scale*2);
	
	ctx.restore();				
	//move clouds
	if(clouds[index].frameCounter==clouds[index].framesToSkip||clouds[index].framesToSkip==0){
		clouds[index].frameCounter=0;
		clouds[index].offset-=clouds[index].speed;
		clouds[index].alpha-=0.0003;
		if(clouds[index].alpha<0.02)
			clouds[index].alpha=0.02;
		
	}
	clouds[index].frameCounter++;
}

var c;
var ctx;
var pano;
var img;

var c_mate;
var ctx_mate;
var pano_mate;

window.onresize = function(event) {
	console.log("RESIZE")
	ctx.canvas.width=pano.clientWidth;
	ctx.canvas.height = ctx.canvas.width*9/16;
	scale=ctx.canvas.width/1920;
};

function pause_rendering_canvas(){
	player.rendering=false;
}

function resume_rendering_canvas(){
	player.rendering=true;
}

function init_rendering_canvas(){
	player.rendering=true;
	console.log("init rendering")
	//INIT CLOUDS
	for(var i=0; i<clouds.length;i++){
		clouds[i].ymax+=clouds[i].imgs[0].height;	
		clouds[i].ymin-=clouds[i].imgs[0].height*1.5;	
		//console.log("index="+i+" ymax="+clouds[i].ymax+" ymin="+clouds[i].ymin);
	}
	
	//INIT CANVAS
	c = document.getElementById("myCanvas");
	ctx = c.getContext("2d");
	
	pano=document.getElementById("pano");
	img = document.getElementById("animation");
	c.addEventListener("mousedown", doMouseDown, false);
	
	ctx.canvas.width=pano.clientWidth;
	ctx.canvas.height = ctx.canvas.width*9/16;
	scale=ctx.canvas.width/1920;
	
	c_mate = document.getElementById("p_mate_canvas");
	ctx_mate = c_mate.getContext("2d");
	p_mate=document.getElementById("p_mate");
	
	//ctx_mate.canvas.width=p_mate.clientWidth;
	//ctx_mate.canvas.height = p_mate.clientHeight;
	
	ctx_mate.canvas.width=p_mate.clientWidth;
	ctx_mate.canvas.height = p_mate.clientHeight;
	
	/*renderInterval=setInterval(
		function(){ 
			ctx.drawImage(img, 0, 0,ctx.canvas.width,ctx.canvas.height);

			for(var k=0; k<POIs.length;k++){
				drawPOI(k,ctx, scale);	
			}
			
			
			for(var i=0; i<clouds.length;i++){
				drawCloud(i,ctx, scale);	
			}
			
			hoverMouseCheck();
			print_FPS();
			
		}, 33); // 30frames per second	*/
	render();
}

/*Handle rendeirng*/
var rendering={
	fps:30,
	interval:1000/30, //1000/fps
	then:0,
	last_loop:0
}
var fps_p;

function print_FPS(){
	var this_loop = new Date;
	var fps = 1000 / (this_loop - rendering.last_loop);
	fps_p.innerHTML="FPS: "+Math.ceil(fps);
	rendering.last_loop = this_loop;
	//console.log("fps: "+fps);
}

function render(){
	
   	requestAnimationFrame(render);
 	
 	var now = Date.now();
    var delta = now - rendering.then;
    
    if (delta > rendering.interval ) { 	//ensuring 30 fps
		
		rendering.then = now - (delta % rendering.interval);
		if(player.rendering){
			ctx.drawImage(img, 0, 0,ctx.canvas.width,ctx.canvas.height);

			for(var k=0; k<POIs.length;k++){
				drawPOI(k,ctx, scale);	
			}
			
			for(var i=0; i<clouds.length;i++){
				drawCloud(i,ctx, scale);	
			}

			hoverMouseCheck();
			print_FPS();
		}
		
	}

	if (delta > rendering.interval/2) { 	
		//ensuring 30 fps
		draw_mate_animation(ctx_mate, scale);
			
	}
	
}

/*INIT REDERING*/
window.onload = function() {
	fps_p=document.getElementById("fps_p");
	update_story_state(0);
	document.onmousemove = function(e){
		cursorHoverX = e.pageX;
		cursorHoverY = e.pageY;
	}

	init_rendering_canvas();
	
	var settings = {};
	settings["startscene"] = 0;
	//settings["startscene"] = "scene_salbunara";
	
	embedpano({swf:"tour.swf", xml:"tour.xml", target:"pano", html5:"prefer", passQueryParameters:true, initvars:settings});
	

	/*add event listeners for full screen change*/
	if (document.addEventListener)
	{
		document.addEventListener('webkitfullscreenchange', exit_full_screen_event, false);
		document.addEventListener('mozfullscreenchange', exit_full_screen_event, false);
		document.addEventListener('fullscreenchange', exit_full_screen_event, false);
		document.addEventListener('MSFullscreenChange', exit_full_screen_event, false);
	}			
}
function hoverMouseCheck(){
	//console.log();
	var clickedX=cursorHoverX;
	var clickedY=cursorHoverY;
	
	for(var k=0; k<POIs.length; k++){
		var dist=Math.sqrt(	Math.pow(clickedX-POIs[k].points[POIs[k].offset][0]*scale,2)+
							Math.pow(clickedY-POIs[k].points[POIs[k].offset][1]*scale+(POIs[k].img.height-30)*scale,2));
		//console.log(clickedX +' : '+clickedY+'='+dist);
		if (dist<60*scale){//k object clicked
			//console.log("hover");
				
			if(POIs[k].status==0)
				document.getElementById("myCanvas").style.cursor="url('./html5/images/cursor_lock.png'), auto";
			else
				document.getElementById("myCanvas").style.cursor="pointer";
			break;
		}
		document.getElementById("myCanvas").style.cursor="url('./html5/images/cursor.png'), auto";
	
		
		//console.log("default");
	}
}

/*click events on canvas*/
function doMouseDown(e){
	
	var clickedX = e.clientX;
	var clickedY = e.clientY;
		
	for(var k=0; k<POIs.length; k++){
		var dist=Math.sqrt(	Math.pow(clickedX-POIs[k].points[POIs[k].offset][0]*scale,2)+
							Math.pow(clickedY-POIs[k].points[POIs[k].offset][1]*scale+(POIs[k].img.height-30)*scale,2));
		console.log(clickedX +' : '+clickedY+'='+dist);
		if (dist<60*scale){//k object clicked
			//document.getElementById("myCanvas").style.display="none";
			console.log("item clicked: "+POIs[k].id);
			
			if(POIs[k].status==1){
				//clearInterval(renderInterval);
				onClickEvent(POIs[k].id, false);
				hide_canvas();
			}
			
			/*FB.ui(
			{
				method: 'share',
				app_id:'145634995501895',	
				href: 'https://developers.facebook.com/docs/',
				},
				// callback
				function(response) {
				if (response && !response.error_message) {
				  alert('Posting completed.');
				} else {
				  alert('Error while posting.');
				}
			});*/
		}
	}
}

function hide_canvas(){
	console.log("hide_canvas");
	document.getElementById("myCanvas").style.visibility="hidden";
	document.getElementById("myCanvas").style.display = 'none';
	//clearInterval(renderInterval);
	pause_rendering_canvas();	
}

function show_canvas(){
	console.log("show_canvas");
	if(document.getElementById("myCanvas").style.visibility!="visible"){
		document.getElementById("p_logo").style.visibility="visible";
		document.getElementById("myCanvas").style.display = 'block';
		document.getElementById("myCanvas").style.visibility = 'visible';
		//start_rendering_canvas();
		resume_rendering_canvas();
	}
}


/*****************************************************************
CHAT INTERFACE
******************************************************************/
function chat_state_next(){
	player.chat_state+=1;
	update_chat_state()
}

function chat_state_set(state){
	player.chat_state=state;
	update_chat_state();
}

function show_chat(){
	if(player.story_state>1){
		document.getElementById("c1").checked=true;
		toggle_chat();
	}
}

function hide_chat(){
	document.getElementById("c1").checked=false;
	toggle_chat();
}

function update_chat_state(){
	switch(player.chat_state){
		case 0:
			document.getElementById("b_m_m_chat").style.visibility="visible";
			document.getElementById("b_m_m_chat_b").style.visibility="hidden";
			document.getElementById("b_m_m_chat").className="b_m_m_chat";
			document.getElementById("b_m_m_chat_1").className="b_m_chat_v1_1";
			document.getElementById("b_m_m_chat_2").className="b_m_chat_v1_2";
			document.getElementById("b_m_m_chat_3").className="b_m_chat_v1_3";
		break;
		
		case 1:
			document.getElementById("b_m_m_chat").style.visibility="visible";
			document.getElementById("b_m_m_chat_b").style.visibility="hidden";
			document.getElementById("b_m_m_chat").className="b_m_m_chat";
			document.getElementById("b_m_m_chat_1").className="b_m_chat_v1_1 b_m_chat_v2_1";
			document.getElementById("b_m_m_chat_2").className="b_m_chat_v1_2 b_m_chat_v2_2";
			document.getElementById("b_m_m_chat_3").className="b_m_chat_v1_3 b_m_chat_v2_3";
		break;
		
		case 2:
			document.getElementById("b_m_m_chat").style.visibility="hidden";
		
			document.getElementById("b_m_m_chat_b").style.visibility="visible";
			document.getElementById("b_m_m_chat_b").className="b_m_m_chat_b_v3";
			document.getElementById("b_m_c_button_profile_b").style.visibility="visible";
		break;
		
		case 3:
			document.getElementById("b_m_m_chat").style.visibility="hidden";
		
			document.getElementById("b_m_m_chat_b").style.visibility="visible";
			document.getElementById("b_m_m_chat_b").className="b_m_m_chat_b_v3 b_m_m_chat_b_v4";
			
			document.getElementById("b_m_c_button_profile_b").style.visibility="hidden";
			
		break;
	}	
}

function toggle_chat() {
	play_sound("animation_effects","./html5/sounds/can.mp3");
	player.chat_state=0;
	update_chat_state();
    
	if(document.getElementById("c1").checked) {
    	console.log("hide_chat()");
		//hide fs chat menu
		document.getElementById("b_m_m_menu_fs").style.visibility="hidden";
		document.getElementById("b_m_m_menu_fs").style.display="none";
		//show caht 
		document.getElementById("b_m_m_menu").style.visibility="visible";
		document.getElementById("b_m_m_menu").style.display="block";
		//show smalll menu
		document.getElementById("b_m_m_chat").style.visibility="visible";
		document.getElementById("b_m_m_chat").style.display="block";	
	}else{
		console.log("hide_chat()");
		//hide fs chat menu
		document.getElementById("b_m_m_menu").style.visibility="hidden";
		document.getElementById("b_m_m_menu").style.display="none";
		//show smalll menu
		document.getElementById("b_m_m_chat").style.visibility="hidden";
		document.getElementById("b_m_m_chat").style.display="none";	
		//show caht 
		document.getElementById("b_m_m_menu_fs").style.visibility="visible";
		document.getElementById("b_m_m_menu_fs").style.display="block";
	}
}

/*****************************************************************
IFRAME
******************************************************************/
function iframe_open(src,full_screen_flag){
	
	console.log("iframe_open");
	if(player.story_state<2){
		//exceptions
		if(src!="watch_trailer.html" && src!="under_construction.html" && src!="share_on_fb.html")
			return;
	}
	if(src=="audio_preview_1.html")
		pause_all_sound();
	if (typeof full_screen_flag === 'undefined') { full_screen_flag = false; }
	
	var iframe=document.getElementById("iframe_stuff");
	
	if(full_screen_flag){
		iframe.className="iframe_stuff_fs";
		//this is video so turn off the ambient & text sound
		pause_all_sound();
	}else{
		iframe.className="iframe_stuff center";
	}
	iframe.src="./html5/iframes/"+src;
	console.log("iframe.src: "+iframe.src);
	iframe.style.display="block";	
	iframe.style.visibility="visible";
	//iframe.load(iframe_loaded()); //not working
}



function iframe_loaded(){
	var iframe=document.getElementById("iframe_stuff");
	iframe.style.display="block";	
	iframe.style.visibility="visible";
}

function iframe_hide(resume_sound, state_change){
	close_menu_drawer();
	if (typeof resume_sound === 'undefined') { resume_sound = false; }
	
	if(resume_sound)
		toggle_sound();

	var iframe=document.getElementById("iframe_stuff");
	iframe.style.visibility="hidden";
	iframe.style.display="none";
	iframe.src="";
	console.log("iframe_hide");
	
	
	console.log("resume_sound:" +resume_sound +"state_change:" +state_change);
	if(state_change){/*on some iframe hide events the state needs to be updated*/
		if(player.story_state==8){ 
			update_story_state(108);
		}else if(player.story_state==14){ 
			update_story_state(114);
		}else if(player.story_state==18 || player.story_state==17){ //the second condition added so that you do not need to finish listening to sound
			update_story_state(19);
			hide_mate(); 
		}else if(player.story_state==19){
			update_story_state(119);
		}else if(player.story_state==21 || player.story_state==22){
			update_story_state(23);
			hide_mate(); 
		}
	}
	/*else if(9<player.story_state && player.story_state<14 &&resume_sound){ //BUG POTENTIAL! HACK state change of of state machine because salbunara beach is not implemented
		var krpano = document.getElementById("krpanoSWFObject");
		krpano.call("loadscene('scene_salbunara')");		
	}*/
	
			
}

function iframe_alert_hide(iframe){
	var iframe=document.getElementById("iframe_alert1");
	iframe.style.visibility="hidden";
	console.log("iframe_alert_hide");
}

function play_sound(id, src){
	var player=document.getElementById(id);
	player.src=src;
	player.play();
}

function open_menu_drawer(){
	document.getElementById("b_m_drawer_open").style.visibility="visible";
}

function close_menu_drawer(){
	if(document.getElementById("b_m_drawer_open").style.visibility==="visible")
		play_sound("animation_effects","./html5/sounds/drawer.mp3");
	document.getElementById("b_m_drawer_open").style.visibility="hidden";
}
function open_pictures(){
	open_menu_drawer();
	console.log("open_pictures");
	play_sound("animation_effects","./html5/sounds/drawer.mp3");
	switch(player.picture_count){
		case 0:
			iframe_open('pictures0.html');
		break;
		case 1:
			iframe_open('pictures1.html');
		break;
	}
}

function open_under_construction(override){
	console.log("open_under_construction");
	if(player.story_state>1 || override==true)
		iframe_open('under_construction.html');
}

function open_audios(){
	open_menu_drawer();
	play_sound("animation_effects","./html5/sounds/drawer.mp3");
	switch(player.audio_count){
		case 0:
			iframe_open('audio0.html');
		break;
		case 1:
			iframe_open('audio1.html');
		break;
	}
}

function open_lyrics(){
	open_menu_drawer();
	play_sound("animation_effects","./html5/sounds/drawer.mp3");
	switch(player.lyrics_count){
		case 0:
			iframe_open('lyrics0.html');
		break;
		case 1:
			iframe_open('lyrics1.html');
		break;
		case 2:
			iframe_open('lyrics2.html');
		break;
	}
}
/*******************************************************************
SIDE MENU INTERFACE
/*******************************************************************/

function on_mouse_over_deep_hunter(){
	console.log("on_mouse_over_deep_hunter: "+player.story_state);
	//switch(state){
		//case "s_m_n_dive_deeper":
	hide_s_m_navigation();
	document.getElementById("s_m_n_dive_deeper").style.visibility="visible";	
}

function on_mouse_out_deep_hunter(){
	console.log("on_mouse_out_deep_hunter: "+player.story_state);
	//switch(state){
		//case "s_m_n_dive_deeper":
	hide_s_m_navigation();
	if(player.story_state<3)
		document.getElementById("s_m_n_deep_hunter").style.visibility="visible";	
	else if(player.story_state<8)
		document.getElementById("s_m_n_back_to_bisevo").style.visibility="visible";	
	else if(player.story_state<15) 
		document.getElementById("s_m_n_nikica_and_jera").style.visibility="visible";	
	else if(player.story_state<20) 
		document.getElementById("s_m_n_lada_lucijo").style.visibility="visible";	
	else
		document.getElementById("s_m_n_barba_jera").style.visibility="visible";	
	//update_story_state(player.story_state);
}


function hide_s_m_navigation(){
	document.getElementById("s_m_n_dive_deeper").style.visibility="hidden";
	document.getElementById("s_m_n_deep_hunter").style.visibility="hidden";
	document.getElementById("s_m_n_back_to_bisevo").style.visibility="hidden";
	document.getElementById("s_m_n_nikica_and_jera").style.visibility="hidden";
	document.getElementById("s_m_n_lada_lucijo").style.visibility="hidden";
	document.getElementById("s_m_n_barba_jera").style.visibility="hidden";
	//document.getElementById("p_button_start_episode").style.visibility="hidden";
}

/*******************************************************************
KRPANO INTERFACE
/*******************************************************************/
function hide_all_new_hotspots(krpano){
	console.log("hide_all_new_hotspots");
	krpano.set("hotspot[otok_hisa_mate_hotspot_new].visible",0);
	krpano.set("hotspot[jere_hiska_hotspot_new].visible",0);
	krpano.set("hotspot[nikica_marija_new].visible",0);
	krpano.set("hotspot[salbunara_hisa_nikica_hotspot_new].visible",0);
	krpano.set("hotspot[lada_lucijo_hiska_hotspot_new].visible",0);
	
	krpano.set("hotspot[mate_racunalnik_hotspot_video].visible",0);
	krpano.set("hotspot[mate_knjiga_leva_hotspot_text].visible",0);
	krpano.set("hotspot[mate_knjiga_leva_hotspot_text].visible",0);
	
	krpano.set("hotspot[mate_knjiga_leva_hotspot_text].visible",0);
	krpano.set("hotspot[mate_racunalnik_hotspot_video].visible",0);
	
	krpano.set("hotspot[salbunara_hisa_nikica_hotspot_new].visible",0);
	
	//nikica marija
	krpano.set("hotspot[nikica_hotspot_video].visible",0);
	krpano.set("hotspot[nikica_vhod_hotspot_photo].visible",0);
	
	//lada lucijo
	krpano.set("hotspot[lada_lucio_hotspot_video].visible",0);
	krpano.set("hotspot[lada_lucijo_sac_hotspot_text].visible",0);

	//greanpa jera
	//krpano.set("hotspot[jere_toyota_hotspot_video].visible",0);
	//krpano.set("hotspot[jere_toyota_hotspot_video].visible",0);
	
}

function lock_all_hotspots(krpano){
	console.log("lock_all_hotspots");
	krpano.set("hotspot[otok_hisa_mate_hotspot_napis].locked",1);
	krpano.set("hotspot[lada_lucijo_hiska_hotspot_napis].locked",1);
	krpano.set("hotspot[jere_hiska_hotspot_napis].locked",1);
	krpano.set("hotspot[niko_odesa_napis].locked",1);
	krpano.set("hotspot[pepe_napis].locked",1);
	krpano.set("hotspot[nikica_marija_napis].locked",1);
	krpano.set("hotspot[tomic_napis].locked",1);
	
	//MATE 
	krpano.set("hotspot[mate_knjiga_leva_hotspot].locked",1);
	krpano.set("hotspot[mate_knjiga_leva_hotspot_lock].locked",1);
	lockedItems[0].locked=1;

	//Salbunara
	krpano.set("hotspot[salbunara_hisa1_hotspot].locked",1);
	krpano.set("hotspot[salbunara_hisa1_hotspot_lock].locked",1);
	
	//nikica marija dvorisce
	//krpano.set("hotspot[nikica_vedro_hotspot_lock].locked",1);
	//krpano.set("hotspot[nikica_vedro_hotspot].locked",1);
	lockedItems[1].locked=1;

	//MANJKA LOCK IKONA ZA ŠTALO/VHOD	
	krpano.set("hotspot[nikica_vhod_hotspot].locked",1);
	krpano.set("hotspot[nikica_vhod_hotspot_lock].locked",1);
	
	//GRENPA JERA 
	krpano.set("hotspot[jere_limone_hotspot_lock].locked",1);
	krpano.set("hotspot[jere_limone_hotspot].locked",1);
	
	//LOCK SAC
	//krpano.set("hotspot[lada_lucijo_sac_hotspot].locked",1);
	//krpano.set("hotspot[lada_lucijo_sac_hotspot_lock].locked",1);
	lockedItems[2].locked=1;



	//krpano.set("hotspot[jere_klesce_hotspot_lock].locked",1);
	//krpano.set("hotspot[jere_limone_hotspot].locked",1);
	
	//krpano.set("hotspot[mate_knjiga_desna_hotspot_lock].locked",1);
	//krpano.set("hotspot[mate_knjiga_srednja_hotspot_lock].locked",1);
	//krpano.set("hotspot[mate_knjiga_leva_hotspot_lock].locked",1);
	//krpano.set("hotspot[salbunara_hisa_nikica_hotspot_napis].locked",0);
	//krpano.set("hotspot[salbunara_plaza_hotspot_napis].locked",0);
	//krpano.set("hotspot[plaza_salbunara_hisa_nikica_hotspot_napis].locked",0);
}

function pause_all_sound(){ //AMBIENT id-parameter is obsolete!
	console.log("pause_all_sound");
	//pause also krpano sound_ch1 if playing
	var krpano = document.getElementById("krpanoSWFObject");
	krpano.call("pausesound(sound_ch1);");
	krpano.call("pausesound(sound_ch2);");
	player.sound_ch1=false;
	player.sound_ch2=false;
	
	player.mate_talking=false;

	if(player.sound_ch2){
		document.getElementById("p_mate").className="p_mate_on ";
	}else{
		document.getElementById("p_mate").className="p_mate_on p_mate_off";
	}

	console.log("pausesound sound_ch1: "+player.sound_ch1);
	console.log("pausesound sound_ch2: "+player.sound_ch2);
	
	//document.getElementById(id).play();
	document.getElementById("s_m_button_sound_si").className="signin s_m_button_sound s_m_button_sound_off";	
	document.getElementById("s_m_button_sound_ca").className="signin s_m_button_sound s_m_button_sound_off";	
	document.getElementById("s_m_button_sound_g").className="signin s_m_button_sound s_m_button_sound_off";	
	document.getElementById("p_mate").className="p_mate_on p_mate_off";
}


function toggle_sound(){ //AMBIENT 
	//pause also krpano sound_ch1 if playing
	var krpano = document.getElementById("krpanoSWFObject");
	krpano.call("pausesoundtoggle(sound_ch1);");
	player.sound_ch1=!player.sound_ch1;
	console.log("toggle_sound sound_ch1: "+player.sound_ch1);
	
	if(player.sound_ch1){
		//document.getElementById(id).play();
		document.getElementById("s_m_button_sound_si").className="s_m_button_sound";
		document.getElementById("s_m_button_sound_ca").className="s_m_button_sound";
		document.getElementById("s_m_button_sound_g").className="s_m_button_sound";
	}else{
		//document.getElementById(id).pause();
		document.getElementById("s_m_button_sound_si").className="signin s_m_button_sound s_m_button_sound_off";
		document.getElementById("s_m_button_sound_ca").className="signin s_m_button_sound s_m_button_sound_off";
		document.getElementById("s_m_button_sound_g").className="signin s_m_button_sound s_m_button_sound_off";
	}
}


function toggle_sound_text(){ //TEXT
	if(!player.pause_sound_ch2_enabled){
		console.log("toggle_sound_text not possible!");
		return;
	}
	console.log("toggle_sound_text: ");
	//pause also krpano sound_ch2 if playing
	var krpano = document.getElementById("krpanoSWFObject");
	//krpano.call("pausesoundtoggle(sound_ch2);");
	player.sound_ch2=!player.sound_ch2;
	console.log("toggle_sound sound_ch2: "+player.sound_ch2);

	if(player.sound_ch2){
		krpano.call("resumesound(sound_ch2);");
		player.mate_talking=true;
		document.getElementById("p_mate").className="p_mate_on ";
	}else{
		krpano.call("pausesound(sound_ch2);");
		document.getElementById("p_mate").className="p_mate_on p_mate_off";
		player.mate_talking=false;
	
	}
}


function stop_sound_ch1(){
	var krpano = document.getElementById("krpanoSWFObject");
	player.sound_ch1=false;
	krpano.call("stopsound(sound_ch1);");
				 
}

function stop_sound_ch2(){
	player.sound_ch2=false;
	var krpano = document.getElementById("krpanoSWFObject");
	krpano.call("stopsound(sound_ch2);");				 
}


/********************************************************************
Browser full screen
********************************************************************/
function exit_full_screen_event()
{
   if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen))
    {
		console.log("exit full screen");
		document.getElementById("iframe_alert1").style.visibility="visible";		
	}
		//document.getElementById("body").style.overflowY="hidden";

}

function toggle_full_screen() {
iframe_alert_hide()
  if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
   (!document.mozFullScreen && !document.webkitIsFullScreen) || (!document.webkitIsFullScreenEnabled)){
    if (document.documentElement.mozRequestFullScreen) {  
      document.documentElement.mozRequestFullScreen();  
    }else if (!document.webkitIsFullScreenEnabled){
	   document.documentElement.webkitRequestFullScreen();
	   console.log("fulll_screen"); 
    } else if (document.documentElement.requestFullScreen) {  
      document.documentElement.requestFullScreen();  
    } else if (document.documentElement.webkitRequestFullScreen) {  
      document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
    }  else if(document.webkitIsFullScreenEnabled){
    	document.documentElement.webkitRequestFullScreen();
    }
  } else {  
    if (document.cancelFullScreen) {  
      document.cancelFullScreen();  
    } else if (document.mozCancelFullScreen) {  
      document.mozCancelFullScreen();  
    } else if (document.webkitCancelFullScreen) {  
      document.webkitCancelFullScreen();  
    } 	//document.getElementById("body").style.overflowY="hidden";

  }  
}


/********************************************************************/
//STORY STATE MACHINE
/********************************************************************/
function jump_to_state(){
	update_story_state(player.episodes[0]);
}

function goto_previous_episode(){
	console.log("goto_previous_episode:"+player.story_state);
		if(player.story_state<4){
			lock_all_POIs();
			show_POI("grandpa_jera");	
			update_story_state(player.episodes[3]);
		}else if(player.story_state<=player.episodes[1]){//epislode 1
			//preset animation parameters
			lock_all_POIs();
			update_story_state(player.episodes[0])
		}else if(player.story_state<=player.episodes[2]){
			lock_all_POIs();
			show_POI("nikica_marija");	
			update_story_state(player.episodes[1]);
		}else if(player.story_state<=player.episodes[3]){
			lock_all_POIs();
			show_POI("lucijo_lada");	
			update_story_state(player.episodes[2]);
		}else{
			lock_all_POIs();
			show_POI("grandpa_jera");	
			update_story_state(player.episodes[3]);
		}
}

function story_state_back(){
	player.story_state-=1;
	if(player.story_state<0)
		player.story_state=0;
	update_story_state(player.story_state);
}

function configure_start_episode_button(episode_num){
	
	var p_button_start_episode=document.getElementById("p_button_start_episode");
	var src="./html5/images/p_button_start_episode_"+episode_num+".png";
	p_button_start_episode.src=src;
	console.log("configure_start_episode_button src: "+p_button_start_episode.src);
	p_button_start_episode.addEventListener("click",  function(){
											if(player.episodes[episode_num-1]==3)
											   update_story_state(4); //hack because the first episode does not have the island entery point
											else if (episode_num==5)
											   update_story_state(3); //hack because the first episode does not have the island entery point
											else
											   update_story_state(player.episodes[episode_num-1]);
												
											}, false);

}

function update_story_state(state){
	close_menu_drawer();

	var krpano = document.getElementById("krpanoSWFObject");
	state=parseInt(state)		
	player.story_state=state;
	console.log("update_story_state: "+player.story_state);
	switch (state){
		case 0: //0-cold start,
			lock_all_POIs();
			
			document.getElementById("p_button_back").style.visibility="hidden";
			document.getElementById("p_button_start_episode").style.visibility="hidden";

			/*show signin view*/
			document.getElementById("side_menu_signin").style.visibility="visible";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="hidden";	
			document.getElementById("s_m_n_dive_deeper").style.visibility="hidden";	
			hide_s_m_navigation();
			
			document.getElementById("p_mate").style.visibility="hidden";
			//show_mate();

			/*bottom menu add signin to hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs b_m_button_news_fs_si"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs b_m_button_chat_fs_si"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs b_m_button_ar_fs_si"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs b_m_button_school_fs_si"; 
			/*hide drawer*/
			document.getElementById("b_m_drawer").style.visibility="hidden";
			document.getElementById("b_m_drawer").style.visibility="hidden";
			document.getElementById("b_m_drawer_open").style.visibility="hidden";
			 
			
		break;
		
		case 1: //1-create acount				
			document.getElementById("p_button_back").style.visibility="hidden";
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="visible";
			document.getElementById("side_menu_game").style.visibility="hidden";
			hide_mate();
			document.getElementById("p_mate").style.visibility="hidden";
	
		
			hide_s_m_navigation();
			
		break;
		
		case 2: //2-game
			hide_all_new_hotspots(krpano);
			/*show game view*/
			document.getElementById("p_button_back").style.visibility="hidden";
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="visible";
			
			document.getElementById("p_mate").style.visibility="visible";
			hide_mate();
	
			/*show side menu navigation*/
			hide_s_m_navigation();
			document.getElementById("s_m_n_deep_hunter").style.visibility="visible";
			
			/*bottom menu remove signin from hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs"; 
			document.getElementById("b_m_drawer").style.visibility="visible"; 
			
			/*play mate intro*/
			setTimeout(function() {
				 //play_sound("animation_mate", "./html5/sounds/mate1.mp3");
				 krpano.call("playsound(sound_ch2, html5/MATE OF TEKST NAD OTOKOM.mp3, 1,js(update_story_state(3)));");
				 //krpano.call("playsound(sound_ch2, html5/can.mp3, 1,js(update_story_state(3)));");
				 player.sound_ch2=true;
				 show_mate();
			 }, player.delay);
			/**/
		break;
		
		case 3://episode 1
			lock_all_POIs();
			show_canvas();
			
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="visible";
			
			hide_s_m_navigation();
			document.getElementById("s_m_n_back_to_bisevo").style.visibility="visible";
			document.getElementById("p_button_back").style.visibility="hidden";
			configure_start_episode_button(1);
			document.getElementById("p_button_start_episode").style.visibility="visible";
			
			
			/*bottom menu remove signin from hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs"; 
			document.getElementById("b_m_drawer").style.visibility="visible"; 
			
			hide_mate();
			krpano.call("loadscene(0)");

			
		break;
		
		case 4: 
			hide_s_m_navigation();
			document.getElementById("p_logo").style.visibility="hidden";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			document.getElementById("s_m_n_back_to_bisevo").style.visibility="visible";
			
			hide_canvas();
			//UI configuraion
			//krpano.call("loadscene(0)");
			krpano.call("loadscene(0)");
			hide_all_new_hotspots(krpano);
			lock_all_hotspots(krpano);
			krpano.set("hotspot[otok_hisa_mate_hotspot_new].visible",1);
			krpano.set("hotspot[otok_hisa_mate_hotspot_napis].locked",0);			/*GORNJE SELO -
			 Matetova hiša ima oznako NEW, vse ostale hiše so LOCK*/
			//krpano.call("loadscene('scene_gornje_selo',plugin[sound_ch1].mute=false&plugin[sound_ch2].mute=false)");
			
			//krpano.call("playsound(sound_ch1, JERE/JERE AMBIENT.mp3, 0); delayedcall(offtext,4,playsound(sound_ch2, JERE/JERE OFF TEKST.mp3, 1))");
		break;	
		
		case 5:
			//krpano.call("showlog()");
			setTimeout(function() {
				show_mate();
			}, 4000);
			lock_all_hotspots(krpano); //need to add leva knjiga to function
			hide_all_new_hotspots(krpano);
			
			/*var krpano = document.getElementById("krpanoSWFObject");					
			krpano.call("loadscene('scene_mate',autorotate.enabled=false&plugin[sound_ch1].mute=true&plugin[sound_ch2].mute=true)");*/
			//Mate pove OFF TEXT, nato se pojavi samo ikona VIDEO, vse tri knjige na hover so LOCK (ni še ikone za LYRICS)
		
		break;
		
		case 6:
			hide_mate();
			krpano.set("hotspot[mate_racunalnik_hotspot_video].visible",1);
			//UNLOCK COMPUTER
		break;
		
		case 7:
			/*open video in iframe*/
			iframe_open('mate_video.html',true);
			pause_all_sound();
			/*modify interface*/
			hide_all_new_hotspots(krpano);
			krpano.set("hotspot[mate_racunalnik_hotspot_video].visible",1);
			krpano.set("hotspot[mate_knjiga_leva_hotspot_text].visible",1);
			
			krpano.set("hotspot[mate_knjiga_leva_hotspot].locked",0);
			krpano.set("hotspot[mate_knjiga_leva_hotspot_lock].locked",0);
			lockedItems[0].locked=0;
		
		break;
		
		case 8:
			/*open book in iframe*/
			iframe_open('lyrics_preview_1.html');
			player.lyrics_count=1;
			/*modify krpano interface*/
		break;

		case 108:
			configure_start_episode_button(2);
			document.getElementById("p_button_start_episode").style.visibility="visible";
		break;

		
		case 9: //Episode 2
			//Episode 2 configuration for annimation
			lock_all_POIs();
			show_POI("nikica_marija");	
			
			krpano.call("loadscene('scene_salbunara')");
			//krpano.call("loadscene('scene_nikica_marija')");
			//krpano.call("loadscene(5)");
			stop_sound_ch1();
			show_canvas();
			//disable defaoult ambient sound play custom ambient sound
			//krpano.call("pausesoundtoggle(sound_ch1);");
			krpano.call("playsound(sound_ch1, GORNJE SELO/GORNJE SELO AMBIENT.mp3, 0);");
			//UI configuraion
			hide_s_m_navigation();
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="visible";
			
			document.getElementById("s_m_n_nikica_and_jera").style.visibility="visible";
			document.getElementById("p_button_back").style.visibility="visible";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			
			/*bottom menu remove signin from hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs"; 
			document.getElementById("b_m_drawer").style.visibility="visible"; 
		break;
		
		case 10:
			document.getElementById("p_logo").style.visibility="hidden";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			document.getElementById("p_button_back").style.visibility="hidden";
			
			//hack to change sound
			stop_sound_ch1();
			krpano.call("playsound(sound_ch1, SALBUNARA/SALBUNARA AMBIENT.mp3, 0);");
			//load salbunara
			hide_all_new_hotspots(krpano);
			krpano.set("hotspot[salbunara_hisa_nikica_hotspot_new].visible",1);
			hide_canvas();
			
		break;
		
		case 11:
			lock_all_hotspots(krpano);
			hide_all_new_hotspots(krpano);
		    setTimeout(function() {
				show_mate();
			}, 4000);
		break;
		
		case 12:
			hide_mate();
			krpano.set("hotspot[nikica_hotspot_video].visible",1);
		break;
		
		case 13:
			krpano.set("hotspot[nikica_vhod_hotspot_photo].visible",1);
			krpano.set("hotspot[nikica_vhod_hotspot_lock].locked",0);
			krpano.set("hotspot[nikica_vhod_hotspot].locked",0);
			lockedItems[1].locked=0;
			iframe_open("nikica_video.html",true);
			pause_all_sound();
		break;
		
		case 14:
			iframe_open("pictures_preview_1.html");
			player.picture_count+=1;
	
		break;
		

		case 114:
			configure_start_episode_button(3);
			document.getElementById("p_button_start_episode").style.visibility="visible";
		break;

		case 15: //episode 3
			show_canvas();
			lock_all_POIs();
			show_POI("lucijo_lada");	
			
			//krpano.call("loadscene(0)");
			krpano.call("loadscene(0)");
			
			hide_s_m_navigation();
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="visible";
			
			document.getElementById("s_m_n_lada_lucijo").style.visibility="visible";
			document.getElementById("p_button_back").style.visibility="visible";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			
			/*bottom menu remove signin from hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs"; 
			document.getElementById("b_m_drawer").style.visibility="visible"; 
		break;
		
		case 16:
			//krpano.call("loadscene(0)");
			krpano.call("loadscene(0)");
			
			hide_all_new_hotspots(krpano);
			lock_all_hotspots(krpano);
			krpano.set("hotspot[lada_lucijo_hiska_hotspot_new].visible",1);
			krpano.set("hotspot[lada_lucijo_hiska_hotspot_napis].locked",0);
			
			document.getElementById("p_logo").style.visibility="hidden";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			document.getElementById("p_button_back").style.visibility="hidden";
			//hack to change sound
			hide_canvas();
			
		break;
		
		case 17:
			//skrij video in lyrics ikono on (cideo click show cideo)
			hide_all_new_hotspots(krpano);
			lock_all_hotspots(krpano);
			
			setTimeout(function() {
				show_mate();
			}, 4000);
		break;
		
		case 18:
			hide_mate();
			//pokazi le vido ikono
			krpano.set("hotspot[lada_lucio_hotspot_video].visible",1);
			
		break;
		
		case 19:
			krpano.set("hotspot[lada_lucijo_sac_hotspot_text].visible",1);
			//UNLOCK OGNJIŠČE MANJAKA
			lockedItems[2].locked=0;
			krpano.set("hotspot[lada_lucijo_sac_hotspot].locked",0);
			krpano.set("hotspot[lada_lucijo_sac_hotspot_lock].locked",0);
			//show the other icon lyrics (on click show lyrics)
		break;
		
		case 119:
			configure_start_episode_button(4);
			document.getElementById("p_button_start_episode").style.visibility="visible";
		break;

		case 20: //episode4
			lock_all_POIs();
			show_POI("grandpa_jera");	
			
			krpano.call("loadscene(0)");
			hide_all_new_hotspots(krpano);
			lock_all_hotspots(krpano);
			krpano.set("hotspot[jere_hiska_hotspot_new].visible",1);
			krpano.set("hotspot[jere_hiska_hotspot_napis].locked",0);
			show_canvas();
			
			
			hide_s_m_navigation();
			document.getElementById("side_menu_signin").style.visibility="hidden";
			document.getElementById("side_menu_create_account").style.visibility="hidden";
			document.getElementById("side_menu_game").style.visibility="visible";
			
			document.getElementById("s_m_n_barba_jera").style.visibility="visible";
			document.getElementById("p_button_back").style.visibility="visible";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			
			/*bottom menu remove signin from hover*/
			document.getElementById("b_m_button_news_fs").className="b_m_button_news_fs"; 
			document.getElementById("b_m_button_chat_fs").className="b_m_button_chat_fs"; 
			document.getElementById("b_m_button_ar_fs").className="b_m_button_ar_fs"; 
			document.getElementById("b_m_button_school_fs").className="b_m_button_school_fs"; 
			document.getElementById("b_m_drawer").style.visibility="visible"; 		
		break;
		
		case 21:
			document.getElementById("p_logo").style.visibility="hidden";
			document.getElementById("p_button_start_episode").style.visibility="hidden";
			document.getElementById("p_button_back").style.visibility="hidden";
			//hack to change sound
			hide_canvas();
			//krpano.set("hotspot[jere_toyota_hotspot_video].visible",0);
			
		break;

//lock_all_hotspots(krpano);
			
		case 22:
			
			hide_mate();
			krpano.set("hotspot[jere_toyota_hotspot_video].visible",1);
		break;

		case 23:
			configure_start_episode_button(5);
			document.getElementById("p_button_start_episode").style.visibility="visible";
			
			//update_story_state(3); //back to beginging
		break;
		
	}
}

function show_mate(){
	console.log("show_mate");
	document.getElementById("p_mate").style.visibility="visible";
	player.sound_ch2=true;	
	player.mate_talking=true;
	player.pause_enabled=true;
	player.pause_sound_ch2_enabled=true;
}

function hide_mate(){
	console.log("hide_mate");
	//document.getElementById("p_mate").style.visibility="hidden";
	player.mate_talking=false;
	player.sound_ch2=false;	
	player.pause_sound_ch2_enabled=false;
}

/***************************************************************************
KRPANO CLICK EVENTS		
/****************************************************************************/
function onClickEvent(name, locked, p2)
{

	//CAST false,true or 0,1 values to 0,1
	//if(locked!=0 || locked!=1){
		if(locked === 'true')
			l=0;
		else if(locked === 'false')
			l=1;
		else if(locked === '1')
			l=1;
		else if(locked === '0')
			l=0;
		else
			l=0; //if null returned we let the clicks go through
	
	/*else{
		l=locked
	}*/
	console.log('Clicke event: '+name + " locked: "+locked +"cast: "+l);
  
  	if(l==0){
	
		//decision making based on hotsopt name clikc
		switch(name){
			case "otok_hisa_mate_hotspot":
			case "otok_hisa_mate_hotspot_napis":
				update_story_state(5);
			break;
			case "mate_racunalnik_hotspot":
			case "mate_racunalnik_hotspot_video":
				update_story_state(7);
			break;
			case "mate_knjiga_leva_hotspot":
			case "mate_knjiga_leva_hotspot_text":
				if(lockedItems[0].locked==0)
					update_story_state(8);
			break;
			case "nikica_marija":
				update_story_state(10);
			break;
			case "salbunara_hisa_nikica_hotspot":
			case "salbunara_hisa_nikica_hotspot_napis":
			case "plaza_salbunara_hisa_nikica_hotspot_napis":
				update_story_state(11);	
			break;
			
			case "nikica_hotspot_video":
			case "nikica_hotspot":
				update_story_state(13);	
			break;
			
			case "nikica_vhod_hotspot":
			case "nikica_vhod_hotspot_photo":
				if(lockedItems[1].locked==0)
					update_story_state(14);	
			break;
		
			case "plaza_salbunara_stol_hotspot_video":
			case "plaza_salbunara_stol_hotspot":
				iframe_open("dive_deeper.html", true); //enak video kot salbunara
				pause_all_sound();
				//dodaj v mauho sound
				player.audio_count=1;
				
			break;	
			
			case "lucijo_lada":
				update_story_state(16);
			break;
			
			case "lada_lucijo_hiska_hotspot":
			case "lada_lucijo_hiska_hotspot_napis":
				update_story_state(17);
			break;
			
			case "lada_lucio_hotspot":
			case "lada_lucio_hotspot_video":
				pause_all_sound();
				iframe_open("lada_lucijo.html", true); //enak video kot salbunara
			break;

			case "lada_lucijo_sac_hotspot_text":
			case "lada_lucijo_sac_hotspot":
				if(lockedItems[2].locked==0){
					iframe_open("lyrics_preview_2.html", false); //enak video kot salbunara
					player.lyrics_count=2;	
				}
			break;	
			
			case "grandpa_jera":
				update_story_state(21);
			break;

			case "jere_hiska_hotspot_napis":
			case "jere_hiska_hotspot":
				setTimeout(function() {
					show_mate();
				}, player.delay);
				hide_all_new_hotspots(krpano);
				lock_all_hotspots(krpano);
			  break;

			case "jere_toyota_hotspot_video":
			case "jere_toyota_hotspot":
				iframe_open("jera_toyota_video.html", true);
				pause_all_sound();
				
			break;

			case "salbunara_plaza_hotspot_napis":
			case "salbunara_plaza_hotspot":
			case "nikica_marija_salbunara_plaza_hotspot_napis":
				setTimeout(function() {
					show_mate();
				}, player.delay);
			break;
		}
	}else{ // because krpano is not consistan on locked status for non controlled elelments
		switch(name){
			case "salbunara_plaza_hotspot_napis":
			case "salbunara_plaza_hotspot":
				setTimeout(function() {
					show_mate();
				}, player.delay);
			break;

			/*hack for problems with krpano this hotspot is always unlocked*/
			/*case "salbunara_hisa_nikica_hotspot":
			case "salbunara_hisa_nikica_hotspot_napis":
			case "plaza_salbunara_hisa_nikica_hotspot_napis":
				update_story_state(11);	
			break;*/
		}
	}
}

function onMouseOverEvent(name, p1, p2)
{
	cursorContentVisible=1;
	//alert(p1 + ": " + p2 + " / " + p3);
	//console.log('OnMouseOver event: '+name);
	//document.getElementById("pano").style.cursor = "copy";
}

function onMouseOutEvent(name, p1, p2)
{
	cursorContentVisible=0;
	//alert(p1 + ": " + p2 + " / " + p3);
	//console.log('OnMouseOut event: '+name);
	//document.getElementById("pano").style.cursor = "none";
}

function b_t_s_hide_all(){
	document.getElementById("b_t_s_button_exit").style.visibility="hidden";
    document.getElementById("b_t_s_button_skip_intro").style.visibility="hidden";
  	document.getElementById("b_t_s_button_latest_cards").style.visibility="hidden";
	document.getElementById("b_t_s_button_send_new_poscard").style.visibility="hidden";

}

function open_back_to_school(){
	if(player.story_state>1){
		pause_all_sound();
		//hide_canvas();
		b_t_s_hide_all();
		
		document.getElementById("b_t_s_button_skip_intro").style.visibility="visible";
	  	document.getElementById("b_t_s_container").style.visibility="visible";
		document.getElementById("b_t_s_container").style.display="block";

		var video = document.getElementById("b_t_s_video");
		video.load();
	    video.play();
	    video.style.visibility="visible";
	    video.style.display="block";
		    
		video.onended = function() {
		    update_b_t_s_state(1);
		};
	}
}

function exit_back_to_school(){
	toggle_sound();
	player.b_t_s_state=0;
	//show_canvas();
	var video = document.getElementById("b_t_s_video");
	video.pause();
	document.getElementById("b_t_s_container").style.visibility="hidden";
	document.getElementById("b_t_s_container").style.display="none";
	document.getElementById("b_t_s_container").className="b_t_s_container_black";
			
}

function update_b_t_s_state(state){
	player.b_t_s_state=state;
	console.log("update_b_t_s_state: "+state);
	switch(player.b_t_s_state){
		case 1:
			b_t_s_hide_all();
			document.getElementById("b_t_s_button_exit").style.visibility="visible";
		    document.getElementById("b_t_s_button_latest_cards").style.visibility="visible";
			document.getElementById("b_t_s_button_send_new_poscard").style.visibility="visible";
			document.getElementById("b_t_s_container").className="b_t_s_container_black b_t_s_placeholder_1";
			document.getElementById("b_t_s_container_back").style.visibility="hidden";
			var video = document.getElementById("b_t_s_video");
			video.pause();
		    video.style.visibility="hidden";
		    video.style.display="none";
		    document.getElementById("b_t_s_new_card_tumb").style.visibility="hidden";

		break;
		
		case 2:
			//document.getElementById("b_t_s_container").className="b_t_s_container_black b_t_s_placeholder_dark_1";
			document.getElementById("b_t_s_container_back").style.visibility="visible";
			document.getElementById("b_t_s_new_card_tumb").style.visibility="hidden";
	
		break;

		case 3:
			//document.getElementById("b_t_s_container").className="b_t_s_container_black b_t_s_placeholder_1";
			document.getElementById("b_t_s_container_back").style.visibility="hidden";
			document.getElementById("b_t_s_new_card_tumb").src="./html5/images/b_t_s_new_card_tumb.png";
			document.getElementById("b_t_s_new_card_tumb").style.visibility="visible";
		break;

		case 4:
			//document.getElementById("b_t_s_container").className="b_t_s_container_black b_t_s_placeholder_dark_1";
			document.getElementById("b_t_s_container_back").style.visibility="visible";
			document.getElementById("b_t_s_new_card_tumb").src="./html5/images/b_t_s_new_card_tumb_dark.png";
			document.getElementById("b_t_s_new_card_tumb").style.visibility="visible";
		break;

	}

}

var start_episode_was_visible=true;
function hide_start_episode(){
	if (document.getElementById("p_button_start_episode").style.visibility==="visible")
		start_episode_was_visible=true;
	else
		start_episode_was_visible=false;
	document.getElementById("p_button_start_episode").style.visibility="hidden";
}

function show_start_episode(){
	if(start_episode_was_visible)
		document.getElementById("p_button_start_episode").style.visibility="visible";
}

function open_latest_cards(){
	document.getElementById("b_t_s_latest_cards_menu").style.visibility="visible";
	console.log("open_latest_cards");
}

function open_card(card_id){
	console.log("open_card player.b_t_s_state: "+player.b_t_s_state);
	
	if(player.b_t_s_state<3)
		update_b_t_s_state(2);
	else
		update_b_t_s_state(4);

	document.getElementById("b_t_s_card").style.visibility="visible";
	document.getElementById("b_t_s_card").style.display="block";
	
	var b_t_s_card_img= document.getElementById("b_t_s_card_img")
	b_t_s_card_img.src="./html5/images/b_t_s_card_front_"+card_id+".png";
	document.getElementById("b_t_s_card_button_read_card").addEventListener("click",  function(){									
											var src="./html5/images/b_t_s_card_back_"+card_id+".png";
											b_t_s_card_img.src=src;
											console.log("b_t_s_card_img: "+b_t_s_card_img.src);
										}, false);
}

function hide_card(){
	if(player.b_t_s_state<3)
		update_b_t_s_state(1);
	else
		update_b_t_s_state(3);
	document.getElementById("b_t_s_card").style.visibility="hidden";
	document.getElementById("b_t_s_card").style.display="none";
	document.getElementById("b_t_s_card_create").style.visibility="hidden";
	document.getElementById("b_t_s_card_create").style.display="none";
	document.getElementById("b_t_s_latest_cards_menu").style.visibility="hidden";
	
}


function open_create_view(){
	update_b_t_s_state(2);
	
	document.getElementById("b_t_s_card_button_send").style.visibility="hidden";
	document.getElementById("b_t_s_card_button_add_island").style.visibility="visible";
	document.getElementById("b_t_s_card_create").style.visibility="visible";
	document.getElementById("b_t_s_card_create").style.display="block";
	document.getElementById("b_t_s_card_img_create").src="./html5/images/b_t_s_card_aaa.png";
}

function open_send_view(){
	update_b_t_s_state(2);
	document.getElementById("b_t_s_card_button_send").style.visibility="visible";
	document.getElementById("b_t_s_card_button_add_island").style.visibility="hidden";
	document.getElementById("b_t_s_card_img_create").src="./html5/images/b_t_s_card_my_island.png";
}

function send_card(){
	update_b_t_s_state(3);
	//play sound
	play_sound("animation_effects","./html5/sounds/kartica.mp3");
	document.getElementById("b_t_s_new_card_tumb").src="./html5/images/b_t_s_new_card_tumb.png";
	document.getElementById("b_t_s_new_card_tumb").style.visibility="visible";
		
	document.getElementById("b_t_s_card").style.visibility="hidden";
	document.getElementById("b_t_s_card").style.display="none";
	document.getElementById("b_t_s_card_create").style.visibility="hidden";
	document.getElementById("b_t_s_card_create").style.display="none";
	document.getElementById("b_t_s_latest_cards_menu").style.visibility="hidden";
}



